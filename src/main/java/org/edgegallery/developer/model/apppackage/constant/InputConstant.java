/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.developer.model.apppackage.constant;

public class InputConstant {
    public static final String TYPE_STRING = "String";

    public static final String TYPE_PASSWORD = "password";

    public static final String TYPE_TEXT = "text";

    public static final int DEFALUT_NETWORK_VLANID = 2000;

    public static final String DEFALUT_PHYSNET = "physnet2";

    public static final String INPUT_NAME_AZ = "az_dc";

    public static final String INPUT_NETWORK_PREFIX = "APP_Plane0";

    public static final String INPUT_NETWORK_POSTFIX = "_Network";

    public static final String INPUT_PHYSNET_POSTFIX = "_Physnet";

    public static final String INPUT_VLANID_POSTFIX = "_VlanId";

    public static final String INPUT_PORT_IP_POSTFIX = "_IP";

    public static final String INPUT_PORT_MASK_POSTFIX = "_MASK";

    public static final String INPUT_PORT_GW_POSTFIX = "_GW";

    public static final String INPUT_PORT_MASK_DEFAULT = "255.255.255.0";
}
