package org.edgegallery.developer.model.application.container;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ContainerAppPodInfo {
    private String podInfo;

    private String podImageInfo;
}
